package ai;

import java.util.ArrayList;

import game.Game;
import game.Utils;

/**
 * IA impl��mentant l'algorithme MIN-MAX
 * On consid��re MAX l'ia, MIN le joueur
 */
public class MinMaxAI implements AIBlueprint {

	//repr��sente le joueur max
	int max;
	//repr��sente le joueur min
	int min;

	//profondeur maximal de notre MinMax
	final int DEEPMAX = 4;

	public MinMaxAI(){
		
	}
	
	public MinMaxAI(int player) {
		max = player;
		min = (player==2)? 1:2;
	}

	public int guess(Game game) {

		return guess(game, max);

	}
	
	public int guess(Game game, int player) {

		max = player;
		min = (player==2)? 1:2;
		int move = 7;

		//on cherche �� faire gagner le joueur, donc �� maximiser cette valeur
		// en initialisant l'utilit�� du coup �� -25, valeur minimum, on s'assure que le moindre coup sera consid��r�� comme meilleur, et repr��sentera bien la r��alit��
		int utilityMove = -100;

		int utilityTemp;
		Game tempo;

		for(int i : AIUtils.allowedMoves(game, max)){

			try{
				tempo = AIUtils.moveCopy(game, max, i);
				utilityTemp = MinValue(tempo, 0);

				if(utilityTemp>=utilityMove){

					move = i;
					utilityMove = utilityTemp;

				} 
			}catch (Exception e) {
				
			}
			

		}
		
		return move;

	}

	private int max(int n1, int n2) {

		return (n1>n2)? n1:n2;

	}

	private int min(int n1, int n2){
		return (n1<n2)? n1:n2;
	}

	/**
	 * 
	 * @param game l'��tat de la partie �� ��tudier
	 * @param idPlayer l'identifiant du joueur dont on veut maximiser le score
	 * @return la valeur du meilleur coup
	 */
	private int MaxValue(Game game, int deep){

		if(deep == DEEPMAX) return game.getUtility(max);

		// sert �� retourner �� l'��tat du jeu ��tudi�� 
		Game tempo;

		//l'utilit�� minimum de ce jeu est -25
		//en initialisant utility �� -25, on s'assure de ne pas retourner une valeur par d��faut qui ne repr��sente pas la r��alit��
		int utility = -100;

		//variable temporaire contenant l'utilit�� d'un plateau
		int utilityTemp;

		for(int i : AIUtils.allowedMoves(game, max)){

			try{
				tempo = AIUtils.moveCopy(game, max, i);
				utilityTemp = tempo.getUtility(max);

				if(!game.isGamePlaying()){
					//si le dernier coup termine le jeu, on retourne l'utilit�� maximum
					return 100;
				}
				else{
					utility = max(utilityTemp,MinValue(game, deep+1));
				}
			}catch (Exception e) {
				
			}

		}


		return utility;
	}




	/**
	 * 
	 * @param game l'��tat de la partie �� ��tudier
	 * @param idPlayer l'identifiant du joueur dont on veut minimiser le score
	 * @return la valeur du meilleur coup  pour baisser le score du  joueur
	 */
	private int MinValue(Game game, int deep){

		if(deep == DEEPMAX) return game.getUtility(min);

		// sert �� retourner �� l'��tat du jeu ��tudi�� 
		Game tempo;

		//l'utilit�� maximum de ce jeu est 25
		//en initialisant utility �� 25, on s'assure de ne pas retourner une valeure par d��faut qui ne repr��sente pas la r��alit��
		int utility = 100;

		//variable temporaire contenant l'utilit�� d'un plateau
		int utilityTemp;

		for(int i : AIUtils.allowedMoves(game, min) ){

			try{
				tempo = AIUtils.moveCopy(game, max, i);
				utilityTemp = tempo.getUtility(max);

				if(!game.isGamePlaying()){
					//si le dernier coup termine le jeu, on retourne l'utilit�� minimum
					return -100;
				}
				else{
					utility = min(utilityTemp,MaxValue(game, deep+1));
				}
			}catch (Exception e) {
				
			}

		}


		return utility;
	}

}
