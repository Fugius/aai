package ai;

import game.Game;

public interface AIBlueprint {
	public int guess(Game game, int player);
}
