package ai;

import java.util.ArrayList;
import game.Game;

/*
 * UniformH, variation de l'algo de recherche uniforme avec heuristique, il est supposé que l'adversaire joue
 * toujours son meilleur coup immédiat
 * 
 * Origine: etat actuel du jeux, heurisitque inconnue
 * 
 * Chaque noeud à pour heuristique : GainAdverseMaximal - gainPotentiel + Goal
 * 
 * Heuritique Négative ou nulle : victoire
 * 
 * Dévellopement du noeud ayant l'heuristique la plus faible jusqu'a atteindre une heuristique negative ou nulle
 * 
 */

public class UniformH implements AIBlueprint {
	
	private ArrayList<Node> tree;
	private ArrayList<Node> devNodes;
	
	private int maxDepth = 4;
	
	public UniformH() {
		tree = new ArrayList<>();
		devNodes = new ArrayList<>();
	}

	@Override
	public int guess(Game game, int player) {
		//reset tree
		tree = new ArrayList<>();
		devNodes = new ArrayList<>();
				
		int guess = -1;
		
		//all moves possible this turn
		ArrayList<Integer> allowedMoves = AIUtils.allowedMoves(game, player);
		
		//if no moves are available, pass
		if (allowedMoves.size() == 0) {
			return 7;
		}
		

		//Origin "score" since we can't find the true one (depends on the parents), could be anything
		int oScore = 25 - AIUtils.getScore(game, player);
		
		//if we already "won"
		if ( oScore <= 0) {
			return allowedMoves.get((int)Math.round(Math.random() * (allowedMoves.size() - 1)));
		}
		
		// set the tree origin
		Node or = new Node(oScore, (Game)game.clone(), null, -1);
		tree.add(or);
		devNodes.add(or);
		
		//find an end point
		int bestH = Integer.MAX_VALUE;
		while (bestH > 0) {		
			if (tree.size() >= Math.pow(6, maxDepth))
				break;
						
			int index = (devNodes.size() == 1) ? 0 : getMinH(player);
			
			//if we can't find a developpable node
			if (index == -1) {
				break;
			}
			
			//current nodes
			Node nd = devNodes.get(index);
			
			//set bestH
			bestH = nd.h;

			//get moves possible
			allowedMoves = AIUtils.allowedMoves(nd.state, player);
			
			//devellop the node
			for (int i : allowedMoves) {
				Game g = AIUtils.moveCopy(nd.state, player, i);
				int bestEnemyMove = AIUtils.bestMove(g, 3 - player);
				
				if (bestEnemyMove > -1) {
					try {
						g.play(bestEnemyMove, 3 - player);
					} catch (Exception e) {
						e.printStackTrace();
					}
				} else {
					ArrayList<Integer> enemyMoves = AIUtils.allowedMoves(g, 3 - player);
					if (enemyMoves.size() > 0) {
						int move = enemyMoves.get((int)Math.round(Math.random() * (enemyMoves.size() - 1)));
						try {
							g.play(move, 3 - player);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
				
				/*boolean eq = false;
				for (Node node : tree) {
					if (node.equals(g)) {
						eq = true;
					}
				}
				
				if (eq) continue;*/
				
				int playerScore = AIUtils.getScore(g, player);
				int enemyScore = AIUtils.getScore(g, 3 - player);
				int remainingScore = 48 - (playerScore + enemyScore);
				int h =  enemyScore - playerScore + remainingScore;
				
				//create the corresponding node
				Node nnd = new Node(h, g, nd, i);
				
				//add node to tree and to developpable if it is
				tree.add(nnd);
				if (AIUtils.allowedMoves(g, player).size() > 0) {
					devNodes.add(nnd);
				}
				
			}
			
			devNodes.remove(index);
		}
		
		
		bestH = Integer.MAX_VALUE;
		Node target = null;
		for (Node d : tree) {
			if (d.h <= bestH) {
				bestH = d.h;
				target = d;
			}
		}
		
		if (target.parent == null) {
			allowedMoves = AIUtils.allowedMoves(game, player);
			int rd = allowedMoves.get((int)Math.round(Math.random() * (allowedMoves.size() - 1)));
			return rd;
		}
		
		while (target.parent.parent != null) {
			target = target.parent;
		}
		
		guess = target.wellPlayed;
				
		return guess;
	}
	
	//Return index of the developpable node with minimum heuristic
	private int getMinH(int player) {
		int bestH = Integer.MAX_VALUE;
		int index = -1;
		
		//find the developpable node with the smallest heuristic
		for (int i = 0; i < devNodes.size(); i++) {
			if (devNodes.get(i).h <= bestH) {
				bestH = devNodes.get(i).h;
				index = i;
			}
		}
				
		return index;
	}
	
	class Node {
		public Game state = null;
		public Node parent = null;
		
		public int wellPlayed = -1;
		public int h = 0;
		
		public Node(int h, Game st, Node parent, int wp) {
			state = st;
			this.h = h;
			this.parent = parent;
			wellPlayed = wp;
		}
		
		public boolean equals(Game state) {
			boolean result = true;
			
			for (int i = 0; i < state.getBoard1().length; i++) {
				if (this.state.getBoard1()[i] != state.getBoard1()[i]) result = false;
				if (this.state.getBoard2()[i] != state.getBoard2()[i]) result = false;
			}
			
			if (this.state.getP1() != state.getP1()) result = false;
			if (this.state.getP2() != state.getP2()) result = false;
			
			return result;
		}
	}

}
