package ai;

import java.util.ArrayList;

import game.Game;

public class AlphaBetaAI implements AIBlueprint {
	
	private ArrayList<Node> tree;
	private final int depth = 2;
	
	public AlphaBetaAI() {
		tree = new ArrayList<>();
	}

	@Override
	public int guess(Game game, int player) {
		tree = new ArrayList<>();
		generateTree(depth, game, player);
		
		Node res = alphaBeta(tree.get(0), Integer.MIN_VALUE, Integer.MAX_VALUE);
		
		while (res.parent.parent != null) {
			res = res.parent;
		}
		
		return res.move;
	}
	
	private Node alphaBeta(Node nd, int alpha, int beta) {
		
		if (nd.children == null) {
			return nd;
		}
		
		Node v = new Node();
		
		if (!nd.typemin) {
			v.value = Integer.MAX_VALUE;
			
			for (Node c : nd.children) {
				v = min(v, alphaBeta(c, alpha, beta));
				if (alpha >= v.value) //cut
					return v;
				beta = min(beta, v);
			}
			
		} else {
			v.value = Integer.MIN_VALUE;
			
			for (Node c : nd.children) {
				v = max(v, alphaBeta(c, alpha, beta));
				if (v.value >= beta) // cut
					return v;
				alpha = max(alpha, v);
			}
		}
		
		return v;
	}
	
	public int min(int v, Node w) {		
		if (v <= w.value)
			return v;
		return w.value;
	}
	
	public int max(int v, Node w) {
		if (v >= w.value)
			return v;
		return w.value;
	}
	
	public Node min(Node v, Node w) {		
		if (v.value <= w.value)
			return v;
		return w;
	}
	
	public Node max(Node v, Node w) {
		if (v.value >= w.value)
			return v;
		return w;
	}
	
	public void generateTree(int depth, Game origin, int player) {
		
		Node o = new Node();
		o.depth = 0;
		o.parent = null;
		o.state = origin;
		o.value = AIUtils.getScore(origin, player);
		
		tree.add(o);
		
		for (int i = 0; i < depth; i++) {
			ArrayList<Node> toExpand = new ArrayList<Node>();
			
			for (Node n : tree) {
				if (n.depth == i) toExpand.add(n);
			}
			
			ArrayList<Node> mins = new ArrayList<>();
			for (Node n : toExpand) {
				//expand maxs
				ArrayList<Integer> moves = AIUtils.allowedMoves(n.state, player);
				for (int m : moves) {
					Node nd = new Node();
					nd.parent = n;
					nd.move = m;
					nd.typemin = true;
					nd.depth = n.depth;
					nd.state = AIUtils.moveCopy(n.state, player, m);
					
					nd.value = AIUtils.getScore(nd.state, player);
					
					if (n.children == null) n.children = new ArrayList<>();
					n.children.add(nd);
					
					mins.add(nd);
				}
			}
			
			ArrayList<Node> maxs = new ArrayList<Node>();
			for (Node n : mins) {
				//expand max
				ArrayList<Integer> moves = AIUtils.allowedMoves(n.state, 3 - player);
				for (int m : moves) {
					Node nd = new Node();
					nd.parent = n;
					nd.move = m;
					nd.typemin = false;
					nd.depth = n.depth + 1;
					nd.state = AIUtils.moveCopy(n.state, 3 - player, m);
					
					nd.value = AIUtils.getScore(nd.state, 3 - player);
					
					if (n.children == null) n.children = new ArrayList<>();
					n.children.add(nd);
					
					maxs.add(nd);
				}
			}
			
			for (Node n : mins) {
				tree.add(n);
			}
			
			for (Node n : maxs) {
				tree.add(n);
			}
		}
	}

	class Node {
		public int depth;
		public boolean typemin = false;
		public int move = -1;
		
		public int value;
		
		public Node parent;
		public ArrayList<Node> children = null;
		
		public Game state = null;
	}
}
