package ai;

import java.util.ArrayList;

import game.Game;

public class AlphaBetaAI implements AIBlueprint {
	
	private ArrayList<Node> tree;
	private final int MaxDepth = 8;
	private int player;
	
	public AlphaBetaAI() {
		tree = new ArrayList<>();
	}

	@Override
	public int guess(Game game, int player) {
		//init
		tree = new ArrayList<>();
		this.player = player;
		
		//tree origin
		Node o = new Node();
		o.parent = null;
		o.depth = 0;
		o.move = -1;
		o.state = game;
		o.typemin = false;
		o.value = AIUtils.getScore(game, player);
		
		tree.add(o);
		
		//alpha beta
		Node res = alphaBeta(tree.get(0), Integer.MIN_VALUE, Integer.MAX_VALUE);
		if (res.typemin) res = res.parent;
		
		while (res.parent.parent != null) {
			res = res.parent;
		}
		
		return res.move;
	}
	
	private Node alphaBeta(Node nd, int alpha, int beta) {
		
		generateChildren(nd, player);
		//System.out.println(nd.children == null ? "NULL" : "NOT NULL");
		
		if (nd.children == null) {
			return nd;
		}
		
		Node v = new Node();
		
		if (!nd.typemin) {
			v.value = Integer.MAX_VALUE;
			
			for (Node c : nd.children) {
				v = min(v, alphaBeta(c, alpha, beta));
				if (alpha >= v.value) //cut
					return v;
				beta = min(beta, v);
			}
			
		} else {
			v.value = Integer.MIN_VALUE;
			
			for (Node c : nd.children) {
				v = max(v, alphaBeta(c, alpha, beta));
				if (v.value >= beta) // cut
					return v;
				alpha = max(alpha, v);
			}
		}
		
		return v;
	}
	
	public int min(int v, Node w) {		
		if (v <= w.value)
			return v;
		return w.value;
	}
	
	public int max(int v, Node w) {
		if (v >= w.value)
			return v;
		return w.value;
	}
	
	public Node min(Node v, Node w) {		
		if (v.value <= w.value)
			return v;
		return w;
	}
	
	public Node max(Node v, Node w) {
		if (v.value >= w.value)
			return v;
		return w;
	}
	
	public void generateChildren(Node parent, int player) {
		
		if (parent.depth >= MaxDepth || parent.children != null) {
			return;
		}
		
		int p = parent.typemin ? 3 - player : player;
		ArrayList<Integer> moves = AIUtils.allowedMoves(parent.state, p);
		
		for (int m : moves) {
			Node nd = new Node();
			nd.depth = parent.depth + 1;
			nd.move = m;
			nd.parent = parent;
			nd.typemin = !parent.typemin;
			nd.state = AIUtils.moveCopy(parent.state, p, m);
			nd.value = AIUtils.getScore(nd.state, p);
			
			if (parent.children == null) parent.children = new ArrayList<>();
			parent.children.add(nd);
			tree.add(nd);
		}

	}

	class Node {
		public int depth;
		public boolean typemin = false;
		public int move = -1;
		
		public int value;
		
		public Node parent;
		public ArrayList<Node> children = null;
		
		public Game state = null;
	}
}
