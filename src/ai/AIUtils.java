package ai;

import java.util.ArrayList;

import game.Game;

public class AIUtils {
	
	public static ArrayList<Integer> allowedMoves(Game g, int player) {
		ArrayList<Integer> moves = new ArrayList<>();
		
		for (int i = 0; i < 6; i++) {
			if (moveCopy(g, player, i) != null)
				moves.add(i);
		}
		
		return moves;
	}
	
	//find the next best move for a player, return -1 if there is none
	public static int bestMove(Game game, int player) {
		
		int bestScore = 0;
		int move = -1;
		
		for (int i = 0; i < 6; i++) {
			Game prediction = moveCopy(game, player, i);
			
			if (prediction == null) continue;
			
			int score = getScore(prediction, player) - getScore(game, player);
			if (score > bestScore) {
				bestScore =  score;
				move = i;
			}
		}
				
		return move;
	}
	
	//get score of a player (represented by a int)
	public static int getScore(Game g, int p) {
		if (p == 1) {
			return g.getP1();
		} else {
			return g.getP2();
		}
	}
		
	//make a copy of the game an make a move
	public static Game moveCopy(Game g, int player, int move) {
		Game copy = (Game) g.clone();
		
		try {
			if (player == 1) {
				copy.P1Play(move);
			} else {
				copy.P2Play(move);
			}
		} catch (Exception e) {
			copy = null;
		}
		
		return copy;
	}
}
