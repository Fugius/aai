package ai;

import java.util.ArrayList;

import game.Game;

public class RushAI implements AIBlueprint {

	//Guess the best play to do;
	@Override
	public int guess(Game game, int player) {
		int guess = -1;
		
		//if we can't make a move, pass
		ArrayList<Integer> allowedMoves = AIUtils.allowedMoves(game, player);
		if (allowedMoves.size() == 0) return 7;
		
		//next best move
		guess = AIUtils.bestMove(game, player);
		
		//if there is not search the next move that minimize the enemy score
		if (guess > -1)
			return guess;
		
		//the minimum best enemy score for a move
		int bestScore = Integer.MAX_VALUE;
		
		//for each move we can do
		for (int i : allowedMoves) {
			//predict what will be the board if we chose this well (if not possible, go to next well)
			Game prediction = AIUtils.moveCopy(game, player, i);
			if (prediction == null) continue;
			
			//predict the enemy best move for the one we just did (if there is none, go to our next move)
			int bestEnemyMove = AIUtils.bestMove(prediction, 3 - player);
			if (bestEnemyMove < 0) continue;
			
			//check if when we chose the 'i' well, the enemy best move is the lowest one (if so update our best move and
			//go to next move
			prediction = AIUtils.moveCopy(prediction, 3 - player, bestEnemyMove);
			int h = AIUtils.getScore(prediction, 3 - player) - AIUtils.getScore(game, 3 - player);
			if (h < bestScore) {
				bestScore = h;
				guess = i;
			}
		}
		
		guess = allowedMoves.get((int) Math.round(Math.random() * (allowedMoves.size() - 1)));
		return guess;
	}
	
	

}
