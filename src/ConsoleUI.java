import ai.AIBlueprint;
import ai.AlphaBetaAI;
import ai.MinMaxAI;
import ai.RushAI;
import ai.UniformH;
import game.Game;
import game.Utils;

public class ConsoleUI {
	
	public ConsoleUI() {
		menuLoop();
	}
	
	private void menuLoop() {
		System.out.println("\n========================AWALE========================");
		int choice = -1;
		int p1 = -1;
		int p2 = -1;
		
		while (choice != 6) {
			System.out.println("\nMenu :\n"
					+ "1. Player\n"
					+ "2. Rush (Moyen)\n"
					+ "3. MinMAx (Difficile, profondeur 4)\n"
					+ "4. UniformH (Tres Difficile, 1300 nodes max)\n"
					+ "5. AlphaBeta (Tres Difficile, profondeur 8)\n"
					+ "6. Quitter\n");
			
			choice = Utils.getPlayerInput("Choisir le joueur 1");
			
			if (choice < 1 || choice > 6) continue;
			if (choice == 6) break;
			p1 = choice;
			
			choice = Utils.getPlayerInput("Choisir le joueur 2 : ");
			
			if (choice < 1 || choice > 6) continue;
			if (choice == 6) break;
			p2 = choice;
			
			gameLoop(p1, p2);
		}
		
		System.out.println("Fin Awale");
		
	}
	
	private void gameLoop(int p1, int p2) {
		
		System.out.println("\n========================DEBUT DE LA PARTIE========================");
		double coin = Math.random();
		
		System.out.println("Le joueur " + (coin > 0.5 ? "1 " : "2 ") + "commence !");
		
		AIBlueprint Player1 = new Player();
		AIBlueprint Player2 = new Player();
		
		switch (p1) {
			case 1:
				Player1 = new Player();
				break;
			case 2:
				Player1 = new RushAI();
				break;
			case 3:
				Player1 = new MinMaxAI();
				break;
			case 4:
				Player1 = new UniformH();
				break;
			case 5: 
				Player1 = new AlphaBetaAI();
				break;
		}
		
		switch (p2) {
			case 1:
				Player2 = new Player();
				break;
			case 2:
				Player2 = new RushAI();
				break;
			case 3:
				Player2 = new MinMaxAI();
				break;
			case 4:
				Player2 = new UniformH();
				break;
			case 5: 
				Player2 = new AlphaBetaAI();
				break;
		}
		
		Game g = new Game();
		g.create();
		
		if (coin <= 0.5) g.pass();
		
		while(g.isGamePlaying()) {
			Utils.printGame(g);
			
			try {
				if (g.getTurn() % 2 == 0) {
					int guess = Player1.guess((Game)g.clone(), 1);
					
					if (guess == 7) {
						g.pass();
						continue;
					}
					
					if (guess == 8) break;
					
					g.P1Play(guess);
					
					if (p1 > 1) System.out.println("L'IA1 a joue le puits " + guess + " !");
				} else {
					int guess = Player2.guess((Game)g.clone(), 2);
					
					if (guess == 7) {
						g.pass();
						continue;
					}
					
					if (guess == 8) break;
					
					g.P2Play(guess);
					if (p2 > 1) System.out.println("L'IA2 a joue le puits " + guess + " !");
				}
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		}
		
		Utils.printGame(g);
		
		if (g.getP1() >= 25) {
			System.out.println("Le joueur 1 gagne, felicitations !");
		} else if (g.getP2() >= 25) {
			System.out.println("Le joueur 2 gagne, felicitations !");
		} else {
			System.out.println("Egalite !");
		}
		
		System.out.println("Le joueur " + (coin > 0.5 ? "1 " : "2 ") + "avait commence !");
		
	}

	public static void main(String[] args) {
		ConsoleUI cui = new ConsoleUI();
	}
	
	class Player implements AIBlueprint {

		@Override
		public int guess(Game game, int player) {
			return Utils.getPlayerInput("Puits a jouer (7 pour passer)");
		}
		
	}

}
