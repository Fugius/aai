import java.util.ArrayList;

import ai.AIUtils;
import ai.AlphaBetaAI;
import ai.MinMaxAI;
import ai.RushAI;
import ai.UniformH;
import game.Game;
import game.Utils;

public class Main {

	public static void main(String[] args) {
		Game g = new Game();
		g.create();
		
		UniformH rai = new UniformH();
		AlphaBetaAI uai = new AlphaBetaAI();
		
		int lastGuessA = -1;
		int lastGuessB = -1;
		
		int sc1 = 0;
		int sc2 = 0;
		for (int i = 0; i < 50; i++) {
			while(g.isGamePlaying()) {
		
				try {
					if (g.getTurn() % 2 == 0) {
						int guess = rai.guess((Game)g.clone(), 1);
						lastGuessA = guess;
						
						if (guess == 7) {
							g.pass();
							if (lastGuessB == 7) break;
						} else {
							g.P1Play(guess);
						}
						
					} else {
						int guess = uai.guess((Game)g.clone(), 2);
						ArrayList<Integer> moves = AIUtils.allowedMoves(g, 2);
						/*int guess = -1;
						if (moves.size() == 0) {
							guess = 7;
						} else {
							guess = moves.get((int) Math.round(Math.random() * (moves.size() - 1)));
						}*/
						lastGuessB = guess;
						
						if (guess == 7) {
							g.pass();
							if (lastGuessA == 7) break;
						} else {
							g.P2Play(guess);
						}
					}
				} catch(Exception e) {
					e.printStackTrace();
					return;
				}
			}
			System.out.println(i);
			if (g.getP1()>=g.getP2()) {
				sc1++;
			} else {
				sc2++;
			}
			
			g = new Game();
			g.create();
			
			lastGuessA = -1;
			lastGuessB = -1;
		}
		
		System.out.println("Score UniformH: " + sc1 + "\nScore AlphaBetaAI: " + sc2 + "\n Win ratio : " + (float)sc1 / (sc1+sc2));
	}

}
