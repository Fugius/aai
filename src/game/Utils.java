package game;

import java.util.Scanner;

public class Utils {
	public static void printGame(Game g) {
		
		//player 1 to play
		if (g.getTurn() % 2 == 0) {
			System.out.println("\n========================Awale tour : " + g.getTurn() + " | JOUEUR 1========================");
			Utils.printArray(Utils.reverse(g.getBoard2()));
			System.out.print(" - Joueur 2\n");
			Utils.printArray(g.getBoard1());
			System.out.print(" - Joueur 1\n\n");

		} else {
			System.out.println("\n========================Awale tour : " + g.getTurn() + " | JOUEUR 2========================");
			Utils.printArray(Utils.reverse(g.getBoard1()));
			System.out.print(" - Joueur 1\n");
			Utils.printArray(g.getBoard2());
			System.out.print(" - Joueur 2\n\n");

		}
		Utils.printArray(new int[] {0, 1, 2, 3, 4, 5});
		System.out.print(" - numero des puits\n");
		
		//Display players seeds
		System.out.println("\nGraines joueur 1 : " + g.getP1());
		System.out.println("Graines joueur 2 : " + g.getP2() + "\n");
		
	}
	
	public static int getPlayerInput(String question) {
		Scanner s = new Scanner(System.in);
		
		System.out.print("\n" + question + " : ");
		int r = s.nextInt();
		
		return r;
	}
	
	public static void printArray(int[] a) {
		System.out.print("[");
		for (int i = 0; i < a.length - 1; i++) {
			System.out.print(a[i] + " ,");
		}
		System.out.print(a[a.length - 1] + " ]");
	}
	
	public static int[] reverse(int[] a) {
		int s = a.length;
		int[] b = new int [s];
		
		for (int i = 0; i < s; i++) {
			b[s - i - 1] = a[i];
		}
		
		return b;
	}
}
