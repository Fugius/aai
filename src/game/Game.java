package game;

import java.util.ArrayList;

import ai.AIUtils;

/*
 * Class containing the core logic of the game
 */

public class Game implements Cloneable {
	//Board of player 1 and board of player 2
	private Board board1 = new Board();
	private Board board2 = new Board();
	
	//scores of each players
	private int score1 = 0;
	private int score2 = 0;
	
	//turn number
	private int turn = 0;
	
	//is the game playing
	boolean playing = true;
	
	public Game() {
		board1.board = new int[6];
		board1.id = 0;
		
		board2.board = new int[6];
		board2.id = 1;
	}
	
	//init the game
	public void create() {
		for (int i = 0; i < 6; i++) {
			board1.board[i] = 4;
			board2.board[i] = 4;
		}
	}
	
	//get players board
	public int[] getBoard1() {
		return board1.board;
	}
	
	public int[] getBoard2() {
		return board2.board;
	}
	
	public Board getBoard1AsBoard(){
		return board1;
	}
	
	public Board getBoard2AsBoard(){
		return board2;
	}
	
	//get players scores
	public int getP1() {
		return score1;
	}
	
	public int getP2() {
		return score2;
	}
	
	//get current turn
	public int getTurn() {
		return turn;
	}
	
	//play a turn, well : the targeted well, board : the board of the playing player
	public int play(int well, Board board)  throws InvalidWellException, StarvingException {
		if (well < 0 || well > 5) {
			throw new InvalidWellException("Invalid well number !");
		}
		
		//save current values
		int[] oldB1 = board1.board.clone();
		int[] oldB2 = board2.board.clone();
		
		//temp score
		int sc = 0;
		
		//reference to the current board
		Board currentBoard = board;
		
		//taking the seeds into your hand
		int seeds = board.board[well];
		
		if(seeds <1 ){
			throw new InvalidWellException("Your well is empty");
		}
		board.board[well] = 0;
		
		//Well index
		int wi = well;
		
		//seeds repartition
		while (seeds > 0) {
			//passing to the next well (but never to the initial one)
			wi = (wi + 1 == well && board.id == currentBoard.id) ? wi+2 : wi+1;
			
			//at the end of a board, switch
			if (wi > 5) {
				currentBoard = (currentBoard.id == board1.id) ? board2 : board1;
				wi = 0;
			}
			
			//put a seed in the current well
			currentBoard.board[wi]++;
			seeds--;
		}
		
		//check if we are in the opponnent board
		if (currentBoard.id != board.id) {
			//check if we can get seeds
			while (currentBoard.board[wi] <= 2 && currentBoard.board[wi] > 0 ) {
				sc += currentBoard.board[wi];
				currentBoard.board[wi] = 0;
				wi--;
				
				if (wi < 0)
					break;
			}
		}
		
		//check if the opponnent board is empty
		int tsum = 0;
		Board eBoard = (board.id == board1.id ) ? board2 : board1;
		for (int w : eBoard.board) {
			tsum += w;
		}
		
		if (tsum == 0) {		
			board1.board = oldB1;
			board2.board = oldB2;
			
			throw new StarvingException("You can't let your enemy starve !");
		}
		
		return sc;
		
	}
	
	//permet d'utiliser la m��thode P1Play ou P2PLay  en fonction de idPlayer
	public void play(int well, int idPlayer) throws InvalidWellException, StarvingException, InvalidPlayer{
		if(idPlayer == 1){
			P1Play(well);
		}
		else if(idPlayer == 2){
			P2Play(well);
		}else{
			throw new InvalidPlayer("invalid player");
		}
	}
	
	//Play for p1 and p2
	public void P1Play(int well) throws InvalidWellException, StarvingException{		
		
		boolean empty = true;
		for (int i = 0; i < 6; i++) {
			Game temp = (Game) this.clone();
			try {
				temp.play(i, temp.getBoard1AsBoard());
			} catch (Exception e) {
				continue;
			}
			
			for (int b : temp.getBoard2()) {
				empty = false;
			}
		}
		
		if (empty) {
			for (int i = 0; i < board1.board.length; i++) {
				score1 += board1.board[i];
				board1.board[i] = 0;
			}
			playing = false;
			return;
		}
		
		int sc = play(well, board1);
		score1  = (sc >= 0) ? score1 + sc : score1;
		
		turn++;
	}
	
	public void P2Play(int well) throws InvalidWellException, StarvingException {		

		boolean empty = true;
		for (int i = 0; i < 6; i++) {
			Game temp = (Game) this.clone();
			
			try {
				temp.play(i, temp.getBoard2AsBoard());
			} catch (Exception e) {
				continue;
			}			
			for (int b : temp.getBoard1()) {
				empty = false;
			}
		}
		
		if (empty) {
			for (int i = 0; i < board2.board.length; i++) {
				score2 += board2.board[i];
				board2.board[i] = 0;
			}
			playing = false;
			return;
		}
		
		int sc = play(well, board2);
		score2  = (sc >= 0) ? score2 + sc : score2;
		
		turn++;
	}
	
	public void pass () {
		turn++;
	}

	//is the game finished ?
	public boolean isGamePlaying() {
		return playing;
	}
	
	//set boards
	public void setBoard1(Board b) {
		board1 = b;
	}
	
	public void setBoard2(Board b) {
		board2 = b;
	}
	
	public void setBoard1(int[] b){
		board1 = new Board(1, b);
	}
	
	public void setBoard2(int[] b){
		board1 = new Board(2, b);
	}
	
	public int getUtility(int idPlayer){
		if(idPlayer == 1) return getP1()-getP2();
		else return getP2()-getP1();
	}
	
	//clone
    public Object clone() {
    	Game clone = new Game();
    	clone.setBoard1(new Board(board1.id, board1.board));
    	clone.setBoard2(new Board(board2.id, board2.board));
    	clone.setP1(score1);
    	clone.setP2(score2);
		return clone;
    }
    
    public void setP1(int a) {
    	score1 = a;
    }
    
    public void setP2(int a) {
    	score2 = a;
    }
    
    public void backup(Game backup){
    	
    	
    	board1 = backup.getBoard1AsBoard();
    	board2 = backup.getBoard2AsBoard();
    	
    	score1 = backup.getP1();
    	score2 = backup.getP2();
    	
    	turn = backup.turn;
    	playing = backup.playing;
    	
    }
}

//represents a board
class Board {
	public int id = 0;
	public int[] board = null;
	
	public Board() {
		
	}
	
	public Board(int id, int[] brd) {
		if (brd == null) {
			board = null;
		} else {
			board = new int[6];
			for (int i = 0; i < board.length; i++) {
				board[i] = brd[i];
			}
		}
		this.id = id;
	}
}

class StarvingException extends Exception {
	private static final long serialVersionUID = 1L;
	
	public StarvingException(String msg) {
		super (msg);
	}
}

class InvalidWellException extends Exception {
	private static final long serialVersionUID = 1L;
	
	public InvalidWellException(String msg) {
		super (msg);
	}
}

class InvalidPlayer extends Exception {
private static final long serialVersionUID = 1L;
	
	public InvalidPlayer(String msg) {
		super (msg);
	}
}
